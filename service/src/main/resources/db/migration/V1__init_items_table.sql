CREATE TABLE items (
    item_type_id UUID PRIMARY KEY,
    quantity     BIGINT    NOT NULL,
    updated_at   timestamp NOT NULL
);
