package com.aardwark.kw.store.service;

import com.aardwark.kw.store.controller.model.ItemQuantityOperationEnum;
import com.aardwark.kw.store.entity.Item;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ItemService {
    
    List<Item> getAllItems();
    
    Optional<Item> getItem(UUID id);

    List<Item> changeItemsQuantities(List<Item> items, ItemQuantityOperationEnum operation);

}
