package com.aardwark.kw.store.service.impl;

import com.aardwark.kw.store.controller.model.ItemQuantityOperationEnum;
import com.aardwark.kw.store.entity.Item;
import com.aardwark.kw.store.repository.ItemRepository;
import com.aardwark.kw.store.service.ItemService;
import com.aardwark.kw.store.service.exception.ItemNotFoundException;
import com.aardwark.kw.store.service.exception.NotEnoughItemQuantityInStockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static java.time.LocalDateTime.now;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Slf4j
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    public ItemServiceImpl(
        ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> getAllItems() {
        List<Item> allItems = itemRepository.findAll();

        log.info("Returned {} items", allItems.size());
        log.trace("All items are: {}", allItems);

        return allItems;
    }

    @Override
    public Optional<Item> getItem(UUID id) {
        Optional<Item> item = itemRepository.findById(id);

        log.debug("Item data from DB: {}", item);

        return item;
    }

    @Override
    public List<Item> changeItemsQuantities(List<Item> items, ItemQuantityOperationEnum operation) {
        switch (operation) {
            case ADD:
                return addItemsToStock(items);
            case REMOVE:
                return removeItemsFromStock(items);
            default:
                throw new IllegalArgumentException(
                        String.format("Item quantity operation with name %s not implemented", operation.name())
                );
        }
    }

    public List<Item> addItemsToStock(List<Item> itemsToUpdate) {
        Map<UUID, Item> itemsById = getItemsById(itemsToUpdate);

        itemsToUpdate.forEach(itemToUpdate -> addItemToStock(itemToUpdate, itemsById));

        return itemRepository.saveAll(itemsById.values());
    }

    @Transactional
    public List<Item> removeItemsFromStock(List<Item> itemsToUpdate) {
        Map<UUID, Item> itemsById = getItemsById(itemsToUpdate);

        itemsToUpdate.forEach(itemToUpdate -> removeItemFromStock(itemToUpdate, itemsById));

        return itemRepository.saveAll(itemsById.values());
    }

    private Map<UUID, Item> getItemsById(List<Item> itemsToUpdate) {
        return itemRepository.findAllById(itemsToUpdate.stream()
                        .map(Item::getItemTypeId)
                        .collect(toList()))
                .stream()
                .collect(toMap(Item::getItemTypeId,
                        identity()));
    }

    private void addItemToStock(Item itemToUpdate,
                                Map<UUID, Item> itemsById) {
        Item itemFromDb = itemsById.get(itemToUpdate.getItemTypeId());

        if (itemFromDb == null) {
            itemToUpdate.setUpdatedAt(now());
            itemsById.put(itemToUpdate.getItemTypeId(), itemToUpdate);
        } else {
            itemFromDb.setUpdatedAt(now());
            itemFromDb.setQuantity(itemFromDb.getQuantity() + itemToUpdate.getQuantity());
        }
    }

    private void removeItemFromStock(Item itemToUpdate,
                                     Map<UUID, Item> itemsById) {
        Item itemFromDb = itemsById.get(itemToUpdate.getItemTypeId());

        if (itemFromDb == null) { throw new ItemNotFoundException(itemToUpdate.getItemTypeId()); }

        Long newItemQuantity = itemFromDb.getQuantity() - itemToUpdate.getQuantity();
        if (newItemQuantity < 0) { throw new NotEnoughItemQuantityInStockException(itemToUpdate.getItemTypeId()); }

        itemFromDb.setUpdatedAt(now());
        itemFromDb.setQuantity(newItemQuantity);
    }

}
