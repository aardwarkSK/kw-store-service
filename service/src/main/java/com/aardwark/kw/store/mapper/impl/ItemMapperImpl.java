package com.aardwark.kw.store.mapper.impl;

import com.aardwark.kw.store.controller.model.ItemDto;
import com.aardwark.kw.store.entity.Item;
import com.aardwark.kw.store.mapper.ItemMapper;

public class ItemMapperImpl implements ItemMapper {

    @Override
    public ItemDto mapToDto(Item entity) {
        return ItemDto.builder()
                      .itemTypeId(entity.getItemTypeId())
                      .quantity(entity.getQuantity())
                      .updatedAt(entity.getUpdatedAt())
                      .build();
    }

    @Override
    public Item mapToEntity(ItemDto item) {
        return Item.builder()
                   .itemTypeId(item.getItemTypeId())
                   .quantity(item.getQuantity())
                   .build();
    }

}
