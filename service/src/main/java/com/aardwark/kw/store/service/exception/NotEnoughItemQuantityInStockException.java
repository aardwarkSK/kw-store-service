package com.aardwark.kw.store.service.exception;

import java.util.UUID;

public class NotEnoughItemQuantityInStockException extends RuntimeException {

    public NotEnoughItemQuantityInStockException(UUID invalidItem) {
        super(String.format("Not enough item quantity in stock for id: %s", invalidItem));
    }

}
