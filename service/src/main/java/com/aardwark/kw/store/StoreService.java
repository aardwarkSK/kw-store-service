package com.aardwark.kw.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.aardwark.kw.store"})
public class StoreService {

    public static void main(String[] args) {
        SpringApplication.run(StoreService.class, args);
    }

}
