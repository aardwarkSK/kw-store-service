package com.aardwark.kw.store.config;

import com.aardwark.kw.store.mapper.ItemMapper;
import com.aardwark.kw.store.mapper.impl.ItemMapperImpl;
import com.aardwark.kw.store.repository.ItemRepository;
import com.aardwark.kw.store.service.ItemService;
import com.aardwark.kw.store.service.impl.ItemServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StoreConfig {

    @Bean
    public ItemMapper mapper() {
        return new ItemMapperImpl();
    }

    @Bean
    public ItemService itemService(final ItemRepository itemRepository) {
        return new ItemServiceImpl(itemRepository);
    }

}
