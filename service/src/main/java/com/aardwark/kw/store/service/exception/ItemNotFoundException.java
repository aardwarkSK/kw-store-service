package com.aardwark.kw.store.service.exception;

import java.util.UUID;

public class ItemNotFoundException extends RuntimeException {

    public ItemNotFoundException(UUID notFoundItem) {
        super(String.format("Item with id %s not found", notFoundItem));
    }

}
