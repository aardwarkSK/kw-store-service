package com.aardwark.kw.store.repository;

import com.aardwark.kw.store.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Spring data repository interface used for access and modify items data in database.
 */
public interface ItemRepository extends JpaRepository<Item, UUID> {

}
