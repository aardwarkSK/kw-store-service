package com.aardwark.kw.store.controller.error_handler;

import com.aardwark.kw.store.service.exception.ItemNotFoundException;
import com.aardwark.kw.store.service.exception.NotEnoughItemQuantityInStockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.Map;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected @NonNull ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
                                                                           @NonNull HttpHeaders headers,
                                                                           @NonNull HttpStatus status,
                                                                           @NonNull WebRequest request) {
        Map<String, String> errors = ex.getBindingResult().getFieldErrors().stream()
                .filter(fieldError -> nonNull(fieldError.getDefaultMessage()))
                .collect(toMap(
                        FieldError::getField,
                        DefaultMessageSourceResolvable::getDefaultMessage)
                );

        return status(BAD_REQUEST).body(errors);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map<Path, String>> handleConstraintViolationException(ConstraintViolationException exception) {

        Map<Path, String> errors = exception.getConstraintViolations().stream()
                .collect(toMap(
                        ConstraintViolation::getPropertyPath,
                        ConstraintViolation::getMessage
                ));

        return status(BAD_REQUEST).body(errors);
    }

    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<String> handleItemNotFoundException(ItemNotFoundException exception) {
        log.info(exception.getMessage());

        return status(NOT_FOUND).body(exception.getMessage());
    }

    @ExceptionHandler(NotEnoughItemQuantityInStockException.class)
    public ResponseEntity<String> handleNotEnoughItemsInStockException(NotEnoughItemQuantityInStockException exception) {
        log.info(exception.getMessage());

        return status(CONFLICT).body(exception.getMessage());
    }

}
