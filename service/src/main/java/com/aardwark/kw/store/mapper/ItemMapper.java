package com.aardwark.kw.store.mapper;

import com.aardwark.kw.store.controller.model.ItemDto;
import com.aardwark.kw.store.entity.Item;

public interface ItemMapper {

    /**
     * Map DB entity to client model
     * @param entity DB entity
     * @return mapped application Dto
     */
    ItemDto mapToDto(Item entity);

    /**
     *  Map client data to DB entity
     * @param item client model
     * @return mapped DB entity
     */
    Item mapToEntity(ItemDto item);

}
