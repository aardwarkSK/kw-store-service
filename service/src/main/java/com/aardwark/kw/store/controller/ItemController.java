package com.aardwark.kw.store.controller;

import com.aardwark.kw.store.controller.api.ItemsApi;
import com.aardwark.kw.store.controller.model.ItemDto;
import com.aardwark.kw.store.controller.model.ItemQuantityOperationEnum;
import com.aardwark.kw.store.entity.Item;
import com.aardwark.kw.store.mapper.ItemMapper;
import com.aardwark.kw.store.service.ItemService;
import com.aardwark.kw.store.service.exception.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RestController
public class ItemController implements ItemsApi {

    private final ItemService itemService;
    private final ItemMapper mapper;

    public ItemController(ItemService itemService, ItemMapper mapper) {
        this.itemService = itemService;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<ItemDto>> getAllItems() {
        log.info("Get all items");
        List<ItemDto> allItems = itemService.getAllItems()
                                            .stream()
                                            .map(mapper::mapToDto)
                                            .collect(toList());
        return new ResponseEntity<>(allItems, OK);
    }

    @Override
    public ResponseEntity<ItemDto> getItem(final UUID id) {
        log.info("Get item id: {}", id);
        return itemService.getItem(id)
                          .map(mapper::mapToDto)
                          .map(ResponseEntity::ok)
                          .orElseThrow(() -> new ItemNotFoundException(id));
    }

    @Override
    public ResponseEntity<List<ItemDto>> changeItemsQuantities(List<ItemDto> itemsDto, ItemQuantityOperationEnum operation) {
        log.info("Changing items quantities with operation {}", operation.name());

        List<Item> currentItems = itemService.changeItemsQuantities(itemsDto.stream()
                                                                            .map(mapper::mapToEntity)
                                                                            .collect(toList()),
                                                                    operation);
        return ResponseEntity.ok(currentItems.stream()
                                             .map(mapper::mapToDto)
                                             .collect(toList()));
    }

}
