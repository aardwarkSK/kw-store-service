package com.aardwark.kw.store.mapper.impl;

import com.aardwark.kw.store.controller.model.ItemDto;
import com.aardwark.kw.store.entity.Item;
import com.aardwark.kw.store.mapper.ItemMapper;
import org.junit.Test;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

public class ItemMapperImplTest {

    public static final UUID ITEM_TYPE_ID = randomUUID();
    public static final long QUANTITY = 2L;
    private final ItemMapper itemMapper = new ItemMapperImpl();
    private final ItemDto itemDto = ItemDto.builder().itemTypeId(ITEM_TYPE_ID).quantity(QUANTITY).build();
    private final Item item = Item.builder().itemTypeId(ITEM_TYPE_ID).quantity(QUANTITY).build();

    @Test
    public void mapToDto_OK() {
        ItemDto itemDtoActual = itemMapper.mapToDto(item);
        assertThat(itemDtoActual).isEqualTo(itemDto);
    }

    @Test
    public void mapToEntity_OK() {
        Item itemActual = itemMapper.mapToEntity(itemDto);
        assertThat(itemActual).isEqualTo(item);
    }

}
