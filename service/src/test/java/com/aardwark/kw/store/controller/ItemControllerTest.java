package com.aardwark.kw.store.controller;

import com.aardwark.kw.store.controller.model.ItemDto;
import com.aardwark.kw.store.entity.Item;
import com.aardwark.kw.store.mapper.impl.ItemMapperImpl;
import com.aardwark.kw.store.service.ItemService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static com.aardwark.kw.store.controller.model.ItemQuantityOperationEnum.*;
import static java.util.UUID.fromString;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    public static final UUID RANDOM_UUID = fromString("de643721-3713-456d-803c-b8530f774a21");
    private final ItemDto itemDto = ItemDto.builder().itemTypeId(RANDOM_UUID).quantity(2L).build();
    private final Item item = Item.builder().itemTypeId(RANDOM_UUID).quantity(2L).build();
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ItemService itemService;
    @SpyBean
    private ItemMapperImpl itemMapper;

    @Test
    public void getAllEmployees_Empty_OK() throws Exception {
        when(itemService.getAllItems()).thenReturn(new ArrayList<>());

        mvc.perform(MockMvcRequestBuilders.get("/items"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void getAllEmployees_OK() throws Exception {
        when(itemService.getAllItems()).thenReturn(Collections.singletonList(item));

        mvc.perform(MockMvcRequestBuilders.get("/items"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[{\"itemTypeId\":\"" + RANDOM_UUID + "\",\"quantity\":2,\"updatedAt\":null}]")));
    }

    @Test
    public void getItem_OK() throws Exception {
        when(itemService.getItem(RANDOM_UUID)).thenReturn(Optional.of(item));

        mvc.perform(MockMvcRequestBuilders.get("/items/" + RANDOM_UUID.toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"itemTypeId\":\"" + RANDOM_UUID + "\",\"quantity\":2,\"updatedAt\":null}")));
    }

    @Test
    public void getItem_ItemDoesNotExist() throws Exception {
        when(itemService.getItem(RANDOM_UUID)).thenReturn(Optional.empty());

        mvc.perform(MockMvcRequestBuilders.get("/items/" + RANDOM_UUID.toString()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("Item with id " + RANDOM_UUID + " not found")));
    }

    @Test
    public void addToStock() throws Exception {
        when(itemService.changeItemsQuantities(Collections.singletonList(item), ADD))
                .thenReturn(Collections.singletonList(item));

        mvc.perform(MockMvcRequestBuilders.put("/items/quantity")
                        .queryParam("operation", ADD.name())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(Collections.singletonList(itemDto)))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"itemTypeId\":\"" + RANDOM_UUID + "\",\"quantity\":2,\"updatedAt\":null}")));
    }

    @Test
    public void removeFromStock() throws Exception {
        when(itemService.changeItemsQuantities(Collections.singletonList(item), REMOVE))
                .thenReturn(Collections.singletonList(item));

        mvc.perform(MockMvcRequestBuilders.put("/items/quantity")
                        .queryParam("operation", REMOVE.name())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(Collections.singletonList(itemDto)))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[{\"itemTypeId\":\"" + RANDOM_UUID + "\",\"quantity\":2,"
                                                           + "\"updatedAt\":null}]")));
        verify(itemService).changeItemsQuantities(Collections.singletonList(item), REMOVE);
    }
}
